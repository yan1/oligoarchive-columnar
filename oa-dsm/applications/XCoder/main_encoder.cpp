#include <iostream>
#include <fstream>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include "Settings/CodecConfiguration.h"
#include "Settings/FileConfiguration.h"
#include "Settings/Constants.h"
#include "Settings/Generic.h"
#include "../../src/Common/Utils.h"
#include "../../src/Common/Logger.h"
#include "../../src/Codec/Encoder.h"
#include "../../src/DataStructure/Extent.h"
#include "../../src/DataStructure/File.h"


using namespace std;
namespace fs = boost::filesystem;

CodecConfiguration CodecConfig;
FileConfiguration FileConfig;

int main(int argc, char **argv) {

    namespace po = boost::program_options;
    bool help{};
    po::options_description description("encoder [options]");
    description.add_options()("help,h", po::bool_switch(&help), "Display help")(
            "filepath,f", po::value<string>(), "File to encode")(
            "params,p", po::value<string>(), "Read parameters from config file")(
            "config,g", po::value<string>(), "Path to configuration file"
    );

    po::command_line_parser parser{argc, argv};
    parser.options(description);
    auto parsed_result = parser.run();
    po::variables_map vm;
    po::store(parsed_result, vm);
    po::notify(vm);

    InitLogger(debug);

    string param_filename;
    string config_filename;
    string input_file_path;

    if (vm.count("params") && vm.count("config") && vm.count("filepath")) {
        input_file_path = vm["filepath"].as<string>();
        param_filename = vm["params"].as<string>();
        config_filename = vm["config"].as<string>();
    } else {
        std::cerr << description << std::endl;
        return 1;
    }

    CodecConfig.init(config_filename);
    CodecConfig.print_configuration(cout);

    std::string input_filename = fs::path(input_file_path).filename().string();
    fs::path base_path(CodecConfig.TMP_ENCODER_DATA_PATH);

    fs::path boost_randomized_file_path = base_path / fs::path(input_filename + ".tmp").filename();
    fs::path boost_ldpc_encoded_file_path = base_path / fs::path(input_filename + ".ldpc").filename();

    std::string randomized_file_path = boost_randomized_file_path.string();
    std::string ldpc_encoded_file_path = boost_ldpc_encoded_file_path.string();

    File fileToEncode(Settings::NUMBER_BLOCKS_PER_EXTENT, Settings::LDPC_DIM);

    fileToEncode.Open(input_file_path);

    std::vector<byte_t> binary;
    fileToEncode.Read(binary);

    /**
     * Randomize each block separately.
     * Write down on file the randomized blocks as a single binary string.
     */
    fileToEncode.Randomize();

    auto blocksList = fileToEncode.BlocksToStrings();
    {
        std::ofstream randFileStream(randomized_file_path);
        for (auto &blockStr: blocksList) {
            randFileStream << blockStr;
        }
    }

    LOG(info) << "Start LDPC encoding" << std::endl;
    /**
     * Call the LDPC encoder on the randomized binary strings.
     * This produces a set of 281600 bits long blocks.
     **/
    std::string ldpc_encoder_input_filename = fs::path(input_file_path + ".tmp").filename().string();
    std::string ldpc_encoder_output_filename = fs::path(input_file_path + ".ldpc").filename().string();
    call_encode_ldpc(CodecConfig, CodecConfig.TMP_ENCODER_DATA_PATH, ldpc_encoder_input_filename, ldpc_encoder_output_filename);

    LOG(info) << "End LDPC encoding" << std::endl;

    LOG(info) << "Reading LDPC blocks" << std::endl;
    /**
     * Load the LDPC encoded blocks from file.
     * Each block is extended to be a multiple of 15 bits.
     * In this case, a block of 281600 (10% redundancy) it will become 281610
     */
    File encodedFile(Settings::NUMBER_BLOCKS_PER_EXTENT, Settings::FINAL_SIZE_LDPC_BLOCK);

    /**
     * Import metadata: it's important so that the same seed can be used to radnomize the blocks
     * */
    encodedFile.ImportMetadata(fileToEncode);

    std::string line;
    std::ifstream blockStream(ldpc_encoded_file_path);
    while (std::getline(blockStream, line)) {
        encodedFile.AppendBlock(line);
    }

    LOG(info) << "Saving parameters for encoded file" << std::endl;
    /**
     * Save the parameters for the encoded file
     **/
    {
        FileConfig.ORIGINAL_FILE_SIZE = encodedFile.GetSzFile();
        FileConfig.RAND_SEED = encodedFile.GetRandSeed();
        FileConfig.MD5 = encodedFile.GetMd5();
        FileConfig.INPUT_FILENAME = encodedFile.GetInputFilename();
        FileConfig.EXT = encodedFile.GetExt();
        FileConfig.N_EXTENTS = encodedFile.GetNExtents();
    }

    LOG(info) << "Initializing encoder";
    /**
     * Inizialize the oligo encoder: load table and set all parameters.
     */
    Codec codec(Settings::NTS, Settings::PREFIX_SIZE, CodecConfig.TABLE_PATH);
    Encoder encoder(codec);

    LOG(info) << "Tables loaded correctly";

    string left_primer_filename = CodecConfig.LEFT_PRIMERS_PATH;
    string right_primer_filename = CodecConfig.RIGHT_PRIMERS_PATH;
    vector<string> left_primers;
    vector<string> right_primers;

//    import_primers(left_primers, left_primer_filename);
//    import_primers(right_primers, right_primer_filename);

//    if( left_primers.size() < encodedFile.GetExtentsList().size() || right_primers.size()< encodedFile.GetExtentsList().size() ){
//        LOG(fatal) << "Not enough primers. Exiting." << std::endl;
//        exit(-1);
//    }

    LOG(debug) << "Generating encoding motifSet" << std::endl;

    size_t primers_idx = 0;
    size_t refIdx = 0;
    size_t extIdx = 0;
    ColumnarSerializer serializer(Settings::BITVALUE_SIZE, Settings::INDEX_LEN);
    for (Extent &extent : encodedFile.GetExtentsList()) {

        std::vector<Motif> motifSet;
        std::vector<Oligo> oligos;

        fs::path output_oligos_files_path(
                CodecConfig.DATA_DIR_PATH + "/" + input_filename + ".EXTENT_" + to_string(extIdx) + //"_" +
                //to_string(primers_idx) +
                ".FASTA");
        output_oligos_files_path.normalize();
        ofstream oligos_file(output_oligos_files_path.c_str());

        std::vector<std::vector<int64_t>> data;
        data = extent.SerializeData(serializer);
        oligos.resize(data[0].size());

        for (size_t mtfIdx = 0; mtfIdx < Settings::NUMBER_OF_MOTIFS; mtfIdx++) {
            encoder.Fill(data[mtfIdx], Settings::PREFIX_SIZE, Settings::NTS);
            encoder.EncodeValuesGroup();
            encoder.GetEncodingMotifs(motifSet);

            for (size_t oIdx = 0; oIdx < motifSet.size(); oIdx++){
                oligos[oIdx].AppendMotif(motifSet[oIdx]);
            }
            motifSet.clear();
        }


        for (auto & oligo : oligos) {
            oligos_file << ">ref" << refIdx << std::endl;
            oligos_file << oligo.toString() <<std::endl;
            refIdx++;
        }

        LOG(info) << "Encoding oligos written in "
                                << CodecConfig.DATA_DIR_PATH + "/" + input_filename + ".EXTENT_" + to_string(extIdx) +
                                   //"_" + to_string(primers_idx) +
                                   ".FASTA"<<std::endl;
        primers_idx++;
        extIdx++;
        encoder.Clear();
    }
    { /** Save parameters for encoded file */
        ofstream out_conf(param_filename);
        FileConfig.print_configuration(out_conf);
        LOG(info) << "Configuration file for encoded data written in " << param_filename;
    }
    return 0;
}