#include <experimental/filesystem>
#include <boost/multiprecision/cpp_int.hpp>
#include <fstream>
#include <exception>
#include "../Common/MD5.h"
#include "File.h"


void File::SetSzFile(size_t size) {
    metadata.szFile = size;
}

void File::SetRandSeed(const std::string& md5Str) {
    boost::multiprecision::uint256_t md5_tmp;
    std::stringstream s_hex, s_dec;

    s_hex << std::hex << md5Str;
    s_hex >> md5_tmp;
    s_dec << std::dec << md5_tmp;

    metadata.randSeed = stoull(s_dec.str().substr(0, 9));
}

void File::SetMd5(const std::string &md5Str) {
    metadata.md5 = md5Str;
}

void File::SetInputFilename(const std::string &filename) {
    metadata.inputFilename = filename;
}

void File::SetExt(const std::string &extention) {
    metadata.ext = extention;
}

void File::SetNExtents(uint32_t n) {
    metadata.nExtents = n;
}

size_t File::GetSzFile() const {
    return metadata.szFile;
}

int64_t File::GetRandSeed() const {
    return metadata.randSeed;
}

const std::string& File::GetMd5() const {
    return metadata.md5;
}

const std::string &File::GetInputFilename() const {
    return metadata.inputFilename;
}

const std::string &File::GetExt() const {
    return metadata.ext;
}

uint32_t File::GetNExtents() const {
    return metadata.nExtents;
}

void File::Write(std::string_view filePath) {
    std::ofstream file(filePath.data());

    std::uint64_t szExtentBytes = N_BLOCKS_PER_EXTENT * BLOCK_SIZE / BITS_PER_BYTE;
    std::uint64_t extIdx = 0;
    for (auto& ext : data) {
        std::uint64_t szToWrite = std::min(metadata.szFile - szExtentBytes * extIdx, szExtentBytes);
        ext.WriteOnFile(file, szToWrite);
        extIdx++;
    }

}

void File::AppendBlock(std::string_view binaryStringBlock) {
    if ( GetRandSeed() == - 1 || GetSzFile() == 0) {
        throw std::exception();
    }
    auto globalBlockIdx = lastWrittenBlock + 1;
    auto N_EXTENTS = data.size();
    if (globalBlockIdx >= 0 && static_cast<size_t>(globalBlockIdx) >= N_EXTENTS * N_BLOCKS_PER_EXTENT) {
        data.emplace_back(Extent(N_BLOCKS_PER_EXTENT, BLOCK_SIZE, GetRandSeed()));
    }
    std::uint64_t extIdx = globalBlockIdx / N_BLOCKS_PER_EXTENT;
    std::uint64_t localBlockIdx = globalBlockIdx % N_BLOCKS_PER_EXTENT;

    data[extIdx].SetBlockFromString(localBlockIdx, binaryStringBlock);
    lastWrittenBlock++;
}

std::vector<std::string> File::BlocksToStrings() {
    std::vector<std::string> stringSet;
    for (auto& ext : data) {
        for (const auto& block : ext.GetBlocks()) {
            stringSet.emplace_back(block.ToString());
        }
    }
    return stringSet;
}

bool File::Open(std::string_view filePath) {
    fileSink.open(filePath.data());
    SetInputFilename(filePath.data());
    SetSzFile(std::experimental::filesystem::file_size(std::experimental::filesystem::path{filePath}));
    auto extension = std::experimental::filesystem::path{filePath}.extension();
    std::string ext = (extension.empty() ? "None" : extension);
    SetExt(ext);
    return fileSink.is_open();
}

void File::Close() {
    fileSink.close();
}

File::File(std::uint32_t N_BLOCKS_PER_EXTENT, std::uint32_t BLOCK_SIZE) : N_BLOCKS_PER_EXTENT(N_BLOCKS_PER_EXTENT), BLOCK_SIZE(BLOCK_SIZE) {}

void File::Randomize() {
    if ( GetRandSeed() == -1 ) {
        throw std::exception();
    }
    for (auto& ext : data) {
        ext.RandomizeBlocks(GetRandSeed());
    }
}

void File::ImportMetadata(File &file) {
    SetInputFilename(file.GetInputFilename());
    SetSzFile(file.GetSzFile());
    SetMd5(file.GetMd5());
    SetRandSeed(GetMd5());
    SetExt(file.GetExt());
    SetNExtents(file.GetNExtents());
}

std::vector<Extent> &File::GetExtentsList() {
    return data;
}


